class Product(object):

    __price = 0.0

    def __init__(self):
        self.set_price(0.0)

    def set_price(self, price):
        self.__price = price

    def get_price(self):
        return self.__price