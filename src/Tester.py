import ClassInfo
import inspect


class Tester(object):

    __new_classes = []
    __tests = {}
    __tests_created = False
    __directory = ''

    def __init__(self, directory):
        old_classes = list(globals())

        self.__directory = directory
        self.__exec_code()

        for item in globals():
            if item not in old_classes:
                self.__new_classes.append(globals()[item])

    def __exec_code(self):
        to_execute = ''

        with open(self.__directory, 'r') as file_lines:
            for line in file_lines:
                to_execute += line
        exec to_execute in globals()

    def create_tests(self):
        for new in self.__new_classes:
            self.__create_tests_from_class(new)
        self.__tests_created = True

    def __create_tests_from_class(self, a_class):
        self.__tests[a_class.__name__] = {}
        info = ClassInfo.ClassInfo(a_class)
        for method in info.methods():
            self.__create_tests_from_method(a_class, method)

    def __create_tests_from_method(self, a_class, method):
        o = a_class()
        m = getattr(o, method)

        if len(inspect.getargspec(m).args) == 1:
            print 'Creating test for %s.%s()' % (a_class.__name__, method)

            def test(junk):
                print 'Executing test on %s.%s()' % (a_class.__name__, method)
                m()
            self.__tests[a_class.__name__][method] = test

    def were_tests_created(self):
        return self.__tests_created

    def execute_tests(self):
        if not self.were_tests_created():
            self.create_tests()
        for key1, tests in self.__tests.iteritems():
            for key2, test in tests.iteritems():
                test('')

    def write_tests(self):
        if not self.were_tests_created():
            self.create_tests()
        for a_class in self.__new_classes:
            self.__write_tests_on_class(a_class)

    def __write_tests_on_class(self, a_class):
        all_tests = []
        for name, a_test in self.__tests[a_class.__name__].iteritems():
            all_tests.append(a_test)

        def test(junk):
            for m in all_tests:
                m('')

        setattr(a_class, test.__name__, test)

if __name__ == "__main__":
    directory = 'C:\Users\WIN7\PycharmProjects\PLPProject\public\classes\Product.py'
    print 'Loading dir=' + directory + ' ...\n'
    t = Tester(directory)
    print 'Creating tests...\n'
    t.create_tests()
    print 'Executing tests...\n'
    t.execute_tests()
    print 'Writing tests...'
    t.write_tests()
    print '''Executing tests using:
        o = Product()
        o.test()'''
    exec "o = Product();o.test()"