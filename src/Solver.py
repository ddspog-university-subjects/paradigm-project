__author__ = 'ddspog'
__project__ = 'PLPProjectExample'

class Event(object):
    events = [] # static

    def __init__(self, action, time):
        self.action = action
        self.time = time
        Event.events.append(self)

    def __cmp__(self, other):
        return cmp(self.time, other.time)

    def run(self):
        print("%.2f: %s" % (self.time, self.action))

    @staticmethod
    def run_events():
        Event.events.sort()
        for e in Event.events:
            e.run()

def create_mc(description):
    class_name = "".join(x.capitalize() for x in description.split())
    def init(self, time):
        Event.__init__(self, description + " [mc]", time)

    globals()[class_name] = \
        type(class_name, (Event,), {'__init__': init})

if __name__ == "__main__":
    descriptions = ["Light on", "Light off", "Water on", "Water off",
                    "Thermostat night", "Thermostat day", "Ring bell"]
    initializations = "ThermostatNight(5.00); LightOff(2.00); \
        WaterOn(3.30); WaterOff(4.45); LightOn(1.00); \
        RingBell(7.00); ThermostatDay(6.00)"
    [create_mc(dsc) for dsc in descriptions]
    exec initializations in globals()
    Event.run_events()