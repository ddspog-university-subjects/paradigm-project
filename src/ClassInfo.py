class ClassInfo(object):

    __some_class = type('Foo', (), {})

    def __init__(self, some_class):
        import types
        if isinstance(some_class, types.StringType):
            some_class = globals()[some_class]
        self.__some_class = some_class

    def methods(self):
        """Return methods from class as a List.

        :returns: all methods from class
        :rtype: list
        """
        import types

        return [method for method in dir(self.__some_class)
                if isinstance(getattr(self.__some_class, method), types.MethodType)]

if __name__ == "__main__":
    Foo = type('Foo', (), {'sum': lambda self, x, y: x + y, 'diff': lambda self, x, y: x - y})
    info = ClassInfo(Foo)

    print info.methods()
